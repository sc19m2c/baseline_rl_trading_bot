from itertools import groupby
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

#TODO there are some stocks with more data than available trading days, could be worth investigating how this happens

data = pd.read_csv("./safe/data.csv")
print(data.head())

def continuous_data(data, plot=False):
    """
    returns the tickers for all stock that we have trading for each trading during the data range
    simple/temporary solution for ensuring the data is clean
    """

    data = data.groupby(['TICKER'])
    size = data.size()
    large = size[size==5598] # this is the number of trading days over the period 
    if plot:
        large.plot(kind = 'bar')
        plt.show()
    return large.index.values


def check_nan_cols(data):
    """
    return column names of data frame with Nan's in
    """
    nan_cols = []
    for col in data.columns.values:
        if data[col].isnull().values.any():
            nan_cols.append(col)

    return nan_cols

def nan_frequency(data, nan_cols):
    """
    returns the number and proportions on nans for each columns
    """
    store = {}
    for col in nan_cols:
        total = data[col].isnull().sum()
        store[col] = {'total': total, "r": total/len(data[col].values)}

    return store


def stock_nan_distribution(data, nan_cols, stocks):
    """
    returns the distribution of nans over stocks
    this verifies if a single stock responsible for the bulk of nans
    """
    nan_data = data[data.isnull().any(axis=1)]
    for col in nan_cols:
        col_nan_idx = nan_data[col].isnull()
        col_nans = nan_data.loc[col_nan_idx]
        ticker_counts = col_nans['TICKER'].value_counts()
        print(col)
        print(ticker_counts.head(50))
      



def stock_nan_list(data, stock):
    stock_data = data.loc[data['TICKER'] == stock]
    stock_nans = stock_data[data.isnull().any(axis=1)]
    stock_nans.to_csv(f'./safe/{stock}_nan.csv')







def buy_hold_returns(data, plot=False, stocks=None, allocation_dist=None):

    if stocks is None:
        stocks = continuous_data(data)

    if allocation_dist is None:
        allocation_dist = np.ones(len(stocks))/len(stocks) # create equally distributed funds

    stock_data = data.loc[data['TICKER'].isin(stocks)].drop(columns=['SHRCLS', 'NUMTRD'])

    nan_cols = check_nan_cols(stock_data)
    print(nan_frequency(stock_data,nan_cols ))
    # stock_nan_list(stock_data, 'AAPL')
    stock_nan_distribution(stock_data, nan_cols, stocks)
    stock_nan_list(stock_data, 'KEY' )
    stock_nan_list(stock_data, 'ATI' )
    stock_nan_list(stock_data, 'USB' )
    stock_nan_list(stock_data, 'WAB' )
    stock_nan_list(stock_data, 'OKE' )
    stock_nan_list(stock_data, 'TTWO' )




buy_hold_returns(data)