import nasdaqdatalink
import pandas as pd



nasdaqdatalink.read_key('./safe/apikey.nasdaq')
# data = nasdaqdatalink.get_table('SHARADAR/SEP', ticker='AAPL')
# print(len(data))
# # print(data)
# # print(data.head())
# nan_data = data[data.isnull().any(axis=1)] 
# print(nan_data.head())


def get_sp500():
    """
    returns the consituents of the spx
    """
    stocks = pd.read_csv('./SP500_constituents.csv')

    return stocks['Symbol'].values


def get_data(ticker):
    """
    returns the daily trading data for a given stock
    """
    data = nasdaqdatalink.get_table('SHARADAR/SEP', ticker=ticker)
    return data


def verify_quandl_data():
    """
    verifies for each stock we have the same ammount of data and that the data contains no NANs
    saves the data for quick future access
    """
    tickers = get_sp500()
    
    final_tickers = []
    for ticker in tickers:
        print(ticker)
        data = get_data(ticker)
        nan_data = data[data.isnull().any(axis=1)] 
        if len(data) == 2639 and len(nan_data) == 0:
            data.to_csv(f'./safe/quandl_stocks/{ticker}.csv')
            final_tickers.append(ticker)

    print(final_tickers)
    print(len(final_tickers))

    return 



verify_quandl_data()


