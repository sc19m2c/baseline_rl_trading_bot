import torch
import pandas as pd



def normalize(slice: torch.Tensor, _max=1, _min=0):
    """
    slice: ohlc + volume over slice length

    notes:
    ohlc scaled independantly of volume
    """
    #slice size : [series length, num_features]
    slice_prices = slice[:,:-1]
    slice_volume = slice[:,-1:]

    min_price = slice_prices.min()
    max_price = slice_prices.max()

    min_volume = slice_volume.min()
    max_volume = slice_volume.max()

    norm_prices = ((slice_prices-min_price)/(max_price-min_price)) * (_max-_min) + _min 
    norm_volume = ((slice_volume-min_volume)/(max_volume-min_volume)) * (_max-_min) + _min

    norm_slice = torch.cat((norm_prices,norm_volume), 1)
    
    return norm_slice


def normalize_pd(slice: pd.DataFrame,
                 _max=1,
                 _min=0,
                 cols_price = ['open', 'high','low', 'close'],
                 cols_vol= ['volume']):
    """
    slice: ohlc + volume over slice length

    notes:
    ohlc scaled independantly of volume
    """
   
    slice_prices = slice[cols_price]
    slice_volume = slice[cols_vol]
    
    min_price = slice_prices.min().min() # get the min per column, and min overall
    max_price = slice_prices.max().max()

    min_volume = slice_volume.min().min()
    max_volume = slice_volume.max().max()
    
    norm_prices = ((slice_prices-min_price)/(max_price-min_price)) * (_max-_min) + _min 
    norm_volume = ((slice_volume-min_volume)/(max_volume-min_volume)) * (_max-_min) + _min

    norm_slice = pd.concat([norm_prices, norm_volume], axis=1, join="inner")
    
    return norm_slice


def standardize(slice):
    """
    slice: ohlc + volume over slice length

    notes:
    ohlc scaled independantly of volume
    """
    
    return


def standardize_pd(slice: pd.DataFrame,
                  cols_price = ['open', 'high','low', 'close'],
                  cols_vol= ['volume']):
    """
    slice: ohlc + volume over slice length

    notes:
    ohlc scaled independantly of volume
    we use the average variance of each ohlc for the standarization
    """
    slice_prices = slice[cols_price]
    slice_volume = slice[cols_vol]
    
    price_var = slice_prices.var().mean()
    price_mean = slice_prices.mean().mean()
    
    volume_var = slice_volume.var().mean()
    volume_mean = slice_volume.mean().mean()
    
    std_prices = ((slice_prices-price_mean)/price_var) 
    std_volume = ((slice_volume-volume_mean)/volume_var)
   
    std_slice = pd.concat([std_prices, std_volume], axis=1, join="inner")
    
    return std_slice

def std_norm_combination(slice: pd.DataFrame,
                 _max=1,
                 _min=0,
                 cols_price = ['open', 'high','low', 'close'],
                 cols_vol= ['volume']):
    """
    normalization removes the significance of price movements, it is therefore better to use standarization for price data
        this is because an asset drop over a peroid of 
    standardization is prone to data with large vairance, therefore it is better to use normalization for volume data
    """ 
    slice_prices = slice[cols_price]
    slice_volume = slice[cols_vol] 
    
    #price standarization
    price_var = slice_prices.var().mean()
    price_mean = slice_prices.mean().mean()  
    std_prices = ((slice_prices-price_mean)/price_var) 
    
    #volume normalization
    min_volume = slice_volume.min().min()
    max_volume = slice_volume.max().max()
    norm_volume = ((slice_volume-min_volume)/(max_volume-min_volume)) * (_max-_min) + _min
    
    #slice reconstruction
    std_norm_slice = pd.concat([std_prices, norm_volume], axis=1, join="inner")
    
    return std_norm_slice


def test_normalize():
    
    slice = torch.rand(5, 5)*100 
    print(slice)
    norm_slice = normalize(slice)
    print(norm_slice)

    return


if __name__ == '__main__':

    test_normalize()