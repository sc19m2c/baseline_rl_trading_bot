import os
import sys
from tomlkit import item
import torch
from torch.utils.data import Dataset, DataLoader
import glob
import pandas as pd
import objsize
import math

from dataloaders.preprocessing import normalize_pd, standardize_pd, std_norm_combination

class PricePredictionDS(Dataset):
    """
    This dataset is for prediciting ohlc, for the next day, given then past N ohlcv info's
    samples must be in the, input: [series_length, feature_dims], pred: [1, (ohlc)]
    samples (input, pred) must be co-normalized 
    
    Note: total size of all stocks data: 722.119283 MB --- print(objsize.get_deep_size(data))
    """
    
    def __init__(self, 
                 tickers, 
                 series_length, 
                 input_features=['open', 'high', 'low', 'close', 'volume'],
                 prediction_features=['open', 'high', 'low', 'close']):
        
        #get the data in memory
        self.data_path = os.path.dirname(os.path.realpath(__file__)).rsplit(os.sep, 2)[0] + '/data/safe/quandl_stocks/'
        self.stock_paths = glob.glob(f"{self.data_path}*.csv")
        self.data = [pd.read_csv(self.stock_paths[i]) for i in range(len(self.stock_paths))]
        
        #initialize internal vars
        self.series_length = series_length
        self.max_data_index = (len(self.data[0])-series_length)-1
        self.num_datapoints = len(self.data)*self.max_data_index
        self.pred_features = prediction_features
        self.input_features = input_features
        
        return

    def __len__(self):
        return self.num_datapoints

    def __getitem__(self, idx):
        
        #determine the dataframe we will get the data from and the index from that datafram
        index_dataframe = int(idx/self.max_data_index)
        item_index = idx - index_dataframe*self.max_data_index
        #get the stock data as a dataframe
        stock_data = self.data[index_dataframe][self.input_features]
        stock_data = stock_data.iloc[item_index:item_index+self.series_length+1] 
        #normalize the data 
        #TODO should make this function selectable
        stock_data = std_norm_combination(stock_data)
        #format to tensors
        inp = torch.Tensor(stock_data.iloc[0:-1].to_numpy())
        pred = torch.Tensor(stock_data[self.pred_features].iloc[-1].to_numpy())

        return inp, pred
    
    

    
    
if __name__ == "__main__":
    
    ds = PricePredictionDS(['A'], 10)
    
    loader = DataLoader(ds, 10, shuffle=True)
    
    sample = next(iter(loader))
    
    print(sample)
    
    print(sample[0].size(), sample[1].size())