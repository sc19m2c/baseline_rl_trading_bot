import os
import sys
rootdir = os.path.dirname(os.path.realpath(__file__)).rsplit(os.sep, 2)[0]
sys.path.insert(0, rootdir)

from dataloaders.lstm_pp_dataloader import PricePredictionDS
from torch.utils.data import DataLoader
from collections import OrderedDict
import torch
from tqdm import tqdm



class LstmForcecastEncoder(torch.nn.Module):
    
    
    def __init__(self,
                 series_length = 50,
                 prediction_features = ['open'],
                 lstm_input_dim = 5,
                 lstm_hidden_dim = 128,
                 lstm_layers = 2,
                 
                 **kwargs):
        
        super(LstmForcecastEncoder, self).__init__()
        
        self.series_length = series_length
        self.prediction_features = prediction_features
        self.lstm_input_dim = lstm_hidden_dim
        self.lstm_hidden_dim = lstm_hidden_dim
        self.lstm_layers = lstm_layers
        self.condig = kwargs
        
        self.lstm_net = torch.nn.LSTM(
            input_size = lstm_input_dim,
            hidden_size = lstm_hidden_dim,
            num_layers = lstm_layers,
            batch_first = True,
            dropout = 0.1
        )
        
        
        return
    
    
    def forward(self, x):
        # x = [batch, series_length, features]
        # y = [batch, lstm_output_dims]
    
        encoding, (h_n, c_n) = self.lstm_net(x) #[ batch_size, lstm_output_dim]
        encoding = encoding[:,-1:,:] # get the last output only 
        
        return encoding
    
    
class PricePredictionNetork(torch.nn.Module):
    
    def __init__(self, 
                 lr=0.0005,
                 mlp_layers = 2,
                 mlp_width = 128,
                 lstm_args = {'lstm_hiddem_dims':128, 'series_length':10, 'prediction_features':['open']},
                 **kwargs):
        
        super(PricePredictionNetork, self).__init__()
        
        self.lr = lr
        self.mlp_layers = mlp_layers
        self.mlp_width = mlp_width
        self.config = kwargs
        
        self.dataset = PricePredictionDS([], lstm_args['series_length'], prediction_features=lstm_args['prediction_features'])
        self.lstm_net = LstmForcecastEncoder(lstm_args, **kwargs)
        
        mlp_net_layers = []
        for i in range(mlp_layers-1):
            mlp_net_layers.append(torch.nn.Linear(lstm_args['lstm_hiddem_dims'], lstm_args['lstm_hiddem_dims'] ))
            mlp_net_layers.append(torch.nn.Tanh())
            
        mlp_net_layers.append(torch.nn.Linear(lstm_args['lstm_hiddem_dims'], 1))
        # mlp_net_layers.append(torch.nn.Tanh())
               
        self.mlp = torch.nn.Sequential(*mlp_net_layers)
        
        self.optimizer = torch.optim.Adam(params = list(self.mlp.parameters())+list(self.lstm_net.parameters()), lr=self.lr)
        self.loss = torch.nn.MSELoss()
        #TODO add scheduller 
            
        return
    
    
    def forward(self, x):
        # = [batch_size, series_length, features]
        # encode the time series 
        series_encoding = self.lstm_net(x) # [batch_size, lstm_output_dim]
        
        # regress over the time series encoding
        predictions = self.mlp(series_encoding) # [batch_size, prediction_dims]
        
        return predictions.squeeze(2)
    
    
    def train_model(self):
        
        
        for epoch in range(self.config['epochs']):
            loader = DataLoader(self.dataset, batch_size=self.config["batch_size"],num_workers=1,shuffle=True)
            
            stats = OrderedDict()
            stats['loss'] = 0
            progress_bar = tqdm(loader, desc='| Epoch {:03d}'.format(1), leave=False, disable=False)

            for i, (input_seq, target_prices) in enumerate(progress_bar):
                self.optimizer.zero_grad()
                
                predictions = self.forward(input_seq)    
                loss = self.loss(predictions, target_prices)
                
                loss.backward()
                self.optimizer.step()
                
                stats['loss'] += loss.item()
                
                progress_bar.set_postfix({key: '{:.4g}'.format(value / (i + 1)) for key, value in stats.items()},refresh=True)
                
            

        
        return
        
        
        


if __name__ == '__main__':
    
    forecaster = PricePredictionNetork(**{
        'epochs':100,
        'batch_size':100
    })
    
    forecaster.train_model()
    