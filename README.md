# DRL Trading
The goal of this project is to train a DRL agent for trading stocks, learning portfolio management and order execution.



Foobar is a Python library for dealing with word pluralization.


# Architecture

State space: OCHL, volume, num_trades, sector embedding, current holdings, average buy in price, cash available, equity

Action space (continous): buy x ammount, sell x ammount, (implicit do nothing = buy 0, sell 0)

Reward function: TBD

![What is this](docs/arch/arch.png)


# LSTM

## To be implimented

- The LSTM will produce a vector representation of the price history, however the best mechanism for this must be determined. The best paramters will be found for each LSTM method below determined by their source task performance
- The best method will then be used as a trial in the DRL trading system. Different processes the learn the price history representation may be more or less transferable to the target task.

- LSTM models:
> - LSTM - for price prediction
> - LSTM autoencoder
> - LSTM autoencoder with predicition loss
- Parameter Options:
> - Train each with 1,2,3 layers. 
> - Train each with lookback 25, 50, 100
> - Train each with lookback (skip 1) 20, 40, 80



# Notes

- initial data source (WRDS - CRSP) was to dirty to clean without utilizing additional data sources; now using Quandl.


# Fun ideas

- We can theortically track all stocks in a distributed fashion
> 1) track K stocks per system, use the softmax weights to rank stock amongst each other
> 2) parse N winning stocks per system into a new round against other winning stocks
> 3) continue this untill we have a target num of trading stock

> Issues
> 1) How do we sell open possitions: we could still fully execute the full architecture to recover final actions, but only execute on sales of stocks not parsed up into later rounds

- We could learn time of year embeddings, this may be more applicable if we can also consider financial reporting dates

- A heirarchical method to track stock at both daily and monthly to get better understanding of trends

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install foobar.

```bash
pip install foobar
```

## Usage

```python
import foobar

```

## Contributing


## License
[MIT](https://choosealicense.com/licenses/mit/)